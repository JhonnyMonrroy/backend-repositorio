@echo off
echo COMPILAR EL FRONTEND
echo Ingresar al directorio del frontend

cd frontend


echo Compilar el frontend

:: npm i

CMD /C quasar build

echo Eliminar los archivos antiguos y mover los archivos compilados al directorio *static*

CMD /C del /f /q ..\src\main\resources\static\*
CMD /C xcopy /Y /S dist\spa\* ..\src\main\resources\static

echo Regresar al directorio anterior

cd ..

echo GENERACIÓN DEL APIDOC

REM Instalar el APIDOC

:: npm install apidoc -g

echo Generar el Apidoc del proyecto

CMD /C apidoc -i src -o src/main/resources/static/apidoc

echo COMPILAR TODO EL PROYECTO

REM Ejecutar el siguiente comando

CMD /C mvn clean install

echo Copiar la base de datos repositorio.sqlite al directorio target generado

CMD /C copy /Y repositorio.sqlite target\

echo EJECUTAR EL PROYECTO

REM Dentro de la carpeta "target" se encuentran 2 archivos:
REM * directorioUIEAI***.jar
REM * repositorio.sqlite

::echo ingresar al directorio y ejecutar el programa

::cd target
::CMD /C java -jar directorioUIE-0.0.1.jar
::cd ..

::echo Desplegar proyecto
::START http://localhost:8080

exit
