package bo.gob.segip.directorioUIE.controllers;

import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import bo.gob.segip.directorioUIE.utils.Scraping;

@RestController
public class NavegarController {
	
	/**
	 * @api {get} /api/v1/navegar/ 1. Obtener los subdirectorios
	 * @apiVersion 0.0.1
	 * @apiName navegarSubDirectorio
	 * @apiGroup Navegar 
	 * @apiQuery {String} url La url del directorio a parsear.
	 * @apiExample {curl} Ejemplo de solicitud:
	 * 	curl -i /api/v1/navegar/?url=http://10.0.20.84:82/
	 * @apiSampleRequest /api/v1/navegar/
	 * @apiSuccessExample {json} Ejemplo de Solicitud Exitosa (200):
	 * 	HTTP/1.1 200 OK
	 * 	{
	 *	    "url_origen": "http://10.0.20.84:82/",
	 * 	    "directorios": [
	 *	        {
	 *	            "fecha": "5/20/2022",
	 *	            "tipo": "dir",
	 *	            "nombre": "SoporteTecnico"
	 *	        },
	 *	        {
	 *	            "fecha": "5/20/2022",
	 *	            "tipo": "dir",
	 *	            "nombre": "SoporteTics"
	 *	        }
	 *	    ]
	 *	}
	 * 
	 * @apiError (404) {json} error Descripción de que no se pudo encontrar la url solicitada
	 * @apiError (409) {json} error Descripción del error
	 * 
	 * @apiErrorExample {json} (404) NotFound-Response:
	 *     HTTP/1.1 404 Not Found
 	 *     {
 	 *       "error": "Descripción de que no se pudo encontrar la url solicitada"
 	 *     }
	 * 
	 * @apiErrorExample {json} (409) Conflict-Response:
	 *     HTTP/1.1 409 Conflict
 	 *     {
 	 *       "error": "Descripción del error"
 	 *     }
	 */
	
	/**
	 * Metodo para obtener los subdirectorios de un repositorio especifico segun una
	 * url
	 * @author jhonny.monrroy 
	 * @param url La suburl a buscar del repositorio
	 */
	@RequestMapping(value = "api/v1/navegar/", method = RequestMethod.GET)
	public ResponseEntity<?> getSubDirectorio(@RequestParam String url) {
		Map<String, Object> response = new HashMap<>();
		response.put("url_origen", url);

		try {
			response.put("directorios", Scraping.obtenerDirectorioList(url));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		} catch (UnknownHostException e) {
			String mensaje = "URL: " + url + " no resuelta.";
			System.out.println(mensaje);
			response.put("mensaje", mensaje);
			response.put("status", 404);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			e.printStackTrace();
			response.put("error", e);
			response.put("status", 409);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CONFLICT);
		}
	}
	
	
	/**
	 * @api {get} /api/v1/navegar/full/ 2. Obtener los subdirectorios en forma recursiva
	 * @apiVersion 0.0.1
	 * @apiName navegarSubDirectorioRecursivamente
	 * @apiGroup Navegar 
	 * @apiQuery {String} url La url del directorio a parsear.
	 * @apiExample {curl} Ejemplo de solicitud:
	 * 	curl -i /api/v1/navegar/full/?url=http://10.0.20.84:82/
	 * @apiSampleRequest /api/v1/navegar/full/
	 * @apiSuccessExample {json} Ejemplo de Solicitud Exitosa (200):
	 * 	HTTP/1.1 200 OK
	 * 	{
		    "url_origen": "http://10.0.20.84:82/",
		    "children": [
		        {
		            "url_origen": "http://10.0.20.84:82/SoporteTecnico",
		            "fecha": "5/20/2022",
		            "tipo": "dir",
		            "children": [
		                {
		                    "url_origen": "http://10.0.20.84:82/SoporteTecnico/PRO-DNTIC-UNEIAI-001_v1.pdf",
		                    "fecha": "5/20/2022",
		                    "tipo": "pdf",
		                    "header": "file",
		                    "label": "PRO-DNTIC-UNEIAI-001_v1.pdf",
		                    "nombre": "PRO-DNTIC-UNEIAI-001_v1.pdf"
		                },
		                ...
		            ],
		            "header": "dir",
		            "label": "SoporteTecnico",
		            "nombre": "SoporteTecnico"
		        },
		        ...
		    ]
		}
		
	 * @apiError (404) {json} error Descripción de que no se pudo encontrar la url solicitada
	 * @apiError (409) {json} error Descripción del error
	 * 
	 * @apiErrorExample {json} (404) NotFound-Response:
	 *     HTTP/1.1 404 Not Found
 	 *     {
 	 *       "error": "Descripción de que no se pudo encontrar la url solicitada"
 	 *     }
	 * 
	 * @apiErrorExample {json} (409) Conflict-Response:
	 *     HTTP/1.1 409 Conflict
 	 *     {
 	 *       "error": "Descripción del error"
 	 *     }
	 */
	
	
	/**
	 * Metodo para obtener los subdirectorios en forma recursiva de un repositorio especifico segun una
	 * url. <br/>
	 * Nota.- Verificar que la recursividad no consuma mucho ram 
	 * @author jhonny.monrroy
	 * @param url La suburl a buscar del repositorio
	 */
	@RequestMapping(value = "api/v1/navegar/full/", method = RequestMethod.GET)
	public ResponseEntity<?> getFullSubDirectorio(@RequestParam String url) {
		Map<String, Object> response = new HashMap<>();
		response.put("url_origen", url);

		try {
			response.put("children", Scraping.obtenerFullDirectorioList(url));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		} catch (UnknownHostException e) {
			String mensaje = "URL: " + url + " no resuelta.";
			System.out.println(mensaje);
			response.put("mensaje", mensaje);
			response.put("status", 404);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			e.printStackTrace();
			response.put("error", e);
			response.put("status", 409);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CONFLICT);
		}
	}
}
