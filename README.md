# PROYECTO DIRECTORIO DE REPOSITORIOS UNIEIA

## REQUERIMIENTOS PRELIMINARES
Se debe tener instalado Node.Js <https://nodejs.org/es/>

Verificar la versión de Node instalada.
```bash
node -v
```
### COMPILAR EL FRONTEND
Ingresar al directorio del frontend
```bash
cd frontend
```

Compilar el frontend

```bash
npm i

quasar build
```

Eliminar los archivos antiguos y 
Mover los archivos compilados al directorio *static*

```bash
rm ..\src\main\resources\static\* -Recurse -Force

mv dist/spa/* ../src/main/resources/static 
```

Regresar al directorio anterior

```bash
cd ..
```

### GENERACIÓN DEL APIDOC

#### Instalar el APIDOC

```bash
npm install apidoc -g
```

#### Generar el Apidoc del proyecto

```bash
apidoc -i src -o src/main/resources/static/apidoc
```

### COMPILAR TODO EL PROYECTO
Ejecutar el siguiente comando

```bash
mvn clean install
```

Copiar la base de datos repositorio.sqlite al directorio target generado

```bash
cp repositorio.sqlite target/
```

### EJECUTAR EL PROYECTO
 Dentro de la carpeta *"target"* se encuentran 2 archivos:
 * directorioUIEAI***.jar
 * repositorio.sqlite

ingresar al directorio y ejecutar el programa

```bash
cd target
java -jar directorioUIEAI***.jar
```

