package bo.gob.segip.directorioUIE.services;

import java.util.List;

import bo.gob.segip.directorioUIE.models.Repositorio;

public interface IRepositorioService {
	public List<Repositorio> getRepositorios();
	public Repositorio getRepositorioById(Long repositorioId);
}
