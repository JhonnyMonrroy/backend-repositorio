package bo.gob.segip.directorioUIE.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bo.gob.segip.directorioUIE.dao.RepositorioDao;
import bo.gob.segip.directorioUIE.models.Repositorio;

@Service
public class RepositorioService implements IRepositorioService {
	@Autowired
	private RepositorioDao repositorioDao;

	@Override
	public List<Repositorio> getRepositorios() {
		return repositorioDao.findAll();
	}

	@Override
	public Repositorio getRepositorioById(Long repositorioId) {
		return repositorioDao.getById(repositorioId);
	}
}
