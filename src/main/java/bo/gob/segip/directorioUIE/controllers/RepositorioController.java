package bo.gob.segip.directorioUIE.controllers;

import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import bo.gob.segip.directorioUIE.models.Repositorio;
import bo.gob.segip.directorioUIE.services.RepositorioService;
import bo.gob.segip.directorioUIE.utils.Scraping;

@RestController
public class RepositorioController {

	@Autowired
	private RepositorioService repositorioService;
	
	
	/**
	 * @api {get} /api/v1/repositorios/ 1. Listar repositorios compatibles
	 * @apiVersion 0.0.1
	 * @apiName listarRepositorios
	 * @apiGroup Repositorios 
	 * @apiExample {curl} Ejemplo de solicitud:
	 * 	curl -i /api/v1/repositorios/
	 * @apiSampleRequest /api/v1/repositorios/
	 * @apiSuccessExample {json} Ejemplo de Solicitud Exitosa (200):
	 * 	HTTP/1.1 200 OK
	 * 	[
		    {
		        "id": 2,
		        "nombre": "Procedimientos",
		        "url": "http://10.0.20.84:82/"
		    },
		    ...
		]
	 * 
	 */
	

	/**
	 * Metodo para para obtener la lista de repositorios compatibles
	 * 
	 * @author jhonny.monrroy
	 */
	@RequestMapping(value = "api/v1/repositorios", method = RequestMethod.GET)
	public List<Repositorio> getRepositorios() {
//		List<Repositorio> lista=repositorioDao.getRepositorios();
//		System.out.println(lista);

//        return lista;
		return repositorioService.getRepositorios();
	}

	/**
	 * @api {get} /api/v1/repositorios/:id 2. Mostrar repositorio
	 * @apiVersion 0.0.1
	 * @apiName mostrarRepositorio
	 * @apiGroup Repositorios 
	 * @apiParam {Number} id Id del repositorio.
	 * @apiParamExample {json} Request-Example:
	 *     {
	 *       "id": 3
	 *     }
	 * @apiExample {curl} Ejemplo de solicitud:
	 * 	curl -i /api/v1/repositorios/2
	 * @apiSuccessExample {json} Ejemplo de Solicitud Exitosa (200):
	 * 	HTTP/1.1 200 OK
	 * 	{
		    "url_origen": "http://10.0.20.84:84/",
		    "directorios": [
		        {
		            "fecha": "5/20/2022",
		            "tipo": "dir",
		            "nombre": "SIS_ASIGNA_NUMERO"
		        },
		        ...
		    ],
		    "titulo": "Contratos Tecnicos"
		}
	 * 
	 */
	
	/**
	 * Metodo para obtener la infromación de un repositorio
	 * 
	 * @author jhonny.monrroy
	 * @param id El Id del Repositorio
	 */
	@RequestMapping(value = "api/v1/repositorios/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getRepositorio(@PathVariable Long id) {
		Repositorio repositorio = repositorioService.getRepositorioById(id);
		// aca se tiene que navegar para obtener el html del la url

		// TODO Hay que verificar que es necesario una funcion especial para navegar por
		// las webs

		Map<String, Object> response = new HashMap<>();

		response.put("url_origen", repositorio.getUrl());

		response.put("titulo", repositorio.getNombre());

		try {
			response.put("directorios", Scraping.obtenerDirectorioList(repositorio.getUrl()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		} catch (UnknownHostException e) {
			String mensaje = "URL: " + repositorio.getUrl() + " no resuelta.";
			System.out.println(mensaje);
			response.put("mensaje", mensaje);
			response.put("status", 404);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			e.printStackTrace();
			response.put("error", e);
			response.put("status", 409);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CONFLICT);
		}
	}
	

	/**
	 * Metodo para obtener los subdirectorios de un repositorio especifico segun s
	 * subruta
	 * 
	 * @param id  el Id del repositorio a buscar
	 * @param dir La suburl a buscar del repositorio
	 */
	@RequestMapping(value = "api/v1/repositorios/{id}/subdir/", method = RequestMethod.GET)
	public ResponseEntity<?> getSubDirectorio(@PathVariable Long id, @RequestParam String dir) {
		Repositorio repositorio = repositorioService.getRepositorioById(id);

		// armamos la URL
		String url_dir = repositorio.getUrl() + dir + "/";

		Map<String, Object> response = new HashMap<>();

		response.put("url_origen", url_dir);

		response.put("titulo", repositorio.getNombre());

		try {
			response.put("directorios", Scraping.obtenerDirectorioList(url_dir));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		} catch (UnknownHostException e) {
			String mensaje = "URL: " + url_dir + " no resuelta.";
			System.out.println(mensaje);
			response.put("mensaje", mensaje);
			response.put("status", 404);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			e.printStackTrace();
			response.put("error", e);
			response.put("status", 409);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CONFLICT);
		}

//		response.put("html", html);
		// TODO: Convertor esto aJSON para poderlo consumir con el frontend

	}

}
