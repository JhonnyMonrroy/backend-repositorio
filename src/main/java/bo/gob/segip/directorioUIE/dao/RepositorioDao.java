package bo.gob.segip.directorioUIE.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import bo.gob.segip.directorioUIE.models.Repositorio;

public interface RepositorioDao extends JpaRepository<Repositorio, Long>{
//	List<Repositorio> getRepositorios();
//
//	Repositorio getRepositorioById(Long id);
}
