package bo.gob.segip.directorioUIE.utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Clase para poder extraer contenido dee un HTML basico expuesto por el file
 * explorer de un servidor IIS
 * 
 * @author jhonny.monrroy
 *
 */
public class Scraping {

	/**
	 * Metodo que obtiene el listado de subdirectorios de una url segun su html
	 * expuesto por los repositorios compatibles
	 * 
	 * @author jhonny.monrroy
	 * @param urldir La url a procesar
	 * @return Un List&lt;Object&gt; con el contenido de los subdirectorios
	 *         encontrados
	 */
	public static List<Object> obtenerDirectorioList(String urldir) throws Exception {
		List<Object> directorios = new ArrayList<Object>();

		urldir = urldir.replace(" ", "%20");
		URL urlObject;
		String codigo;
		String html = "";
		try {

			setTrustAllCerts();
			urlObject = new URL(urldir);

			InputStreamReader isr = new InputStreamReader(urlObject.openStream());
			BufferedReader br = new BufferedReader(isr);
			while ((codigo = br.readLine()) != null) {
//				System.out.println(codigo);
				html += codigo;
			}
			br.close();

			Document doc = Jsoup.parse(html);
			// busco todos los enlaces
			Elements entradas = doc.select("pre");
			Element elemento = entradas.first();
			String textos = elemento.html();// .text();
//			System.out.println(textos);

			String[] lineasHTML = textos.split("<br>");

//			Scanner leerHTML=new Scanner(textos);
			for (String linea : lineasHTML) {
//				System.out.println(linea);

				Document docLinea = Jsoup.parse(linea);
				linea = docLinea.text();
				// omitir la [To Parent Directory]
				if (!linea.contains("[") && !linea.contains("]")) {

					Map<String, Object> item = new HashMap<>();
					// en un principio se saca el enlace en el nombre del archivo
					Element elem = docLinea.select("a").first();
					if (elem != null) {
						item.put("nombre", elem.text());
//						System.out.println(elem.text());
					}

					StringTokenizer leerLinea = new StringTokenizer(linea);
					while (leerLinea.hasMoreTokens()) {
						String cadena = leerLinea.nextToken();

						if (cadena.contains("/")) {
							item.put("fecha", cadena);
						}
//							else if (cadena.contains(":")) {
//								item.put("hora", cadena);
//							} 
//							else if (cadena.equals("PM") || cadena.equals("AM")) {
//								item.put("hora", item.get("hora") + " " + cadena);
//							} 
						else if (cadena.equals("<dir>")) {
							item.put("tipo", "dir");
						} else {
							// item.put("nombre", cadena);
							// asignamos el tipo segun tipo de archivo si es que no es un directorio
							if (item.get("dir") == null) {
								if (cadena.endsWith(".pdf") || cadena.endsWith(".PDF"))
									item.put("tipo", "pdf");
								else if (cadena.endsWith(".docx") || cadena.endsWith(".doc") || cadena.endsWith(".DOCX")
										|| cadena.endsWith(".DOC"))
									item.put("tipo", "doc");
								else if (cadena.endsWith(".txt") || cadena.endsWith(".TXT"))
									item.put("tipo", "txt");
								else if (cadena.endsWith(".rar") || cadena.endsWith(".RAR"))
									item.put("tipo", "rar");
								else if (cadena.endsWith(".ppt") || cadena.endsWith(".pptx") || cadena.endsWith(".PPT")
										|| cadena.endsWith(".PPTX"))
									item.put("tipo", "ppt");
								else if (cadena.toLowerCase().endsWith(".mpg") || cadena.toLowerCase().endsWith(".mp4")
										|| cadena.toLowerCase().endsWith(".wmv")
										|| cadena.toLowerCase().endsWith(".mpg"))
									item.put("tipo", "video");
								else if (cadena.toLowerCase().endsWith(".jpg") || cadena.toLowerCase().endsWith(".gif")
										|| cadena.toLowerCase().endsWith(".png")
										|| cadena.toLowerCase().endsWith(".jpeg"))
									item.put("tipo", "imagen");
								else if (cadena.endsWith(".config") || cadena.endsWith(".vsdx"))
									item.put("tipo", "sistema");
							}
						}

					}
//					System.out.println("---> -->");
//					System.out.println(item);
//					System.out.println("¿Es vacio? " + item.isEmpty());
					// se omite los archivos de sistemas que no se pueden leer
					if (!item.isEmpty() && item.get("tipo") != null && !item.get("tipo").equals("sistema"))
						directorios.add(item);
				}

			}
		} catch (FileNotFoundException e) {
			System.out.println("No se pudo leer el archivo solicitado en URL: " + urldir);
			return new ArrayList<Object>();
		} catch (Exception e) {
//			e.printStackTrace();
			throw e;
		}
		return directorios;

	}

	/**
	 * Metodo que obtiene el listado de subdirectorios en forma recursiva de una url
	 * segun su html expuesto por los repositorios compatibles
	 * 
	 * @author jhonny.monrroy
	 * @param urldir La url a procesar
	 * @return Un List&lt;Object&gt; con el contenido de los subdirectorios
	 *         encontrados
	 */
	public static List<Object> obtenerFullDirectorioList(String urldir) throws Exception {
		List<Object> directorios = new ArrayList<Object>();

		urldir = urldir.replace(" ", "%20");
		// ignoramos el directorio node_modules y retornamos directorio vacio y htmls con scripts
		if (urldir.contains("node_modules")) {
			System.out.println("Ingnoramos el url: " + urldir);
			return new ArrayList<Object>();
		}

		// System.out.println("URL: " + urldir);
		URL urlObject;
		String codigo;
		String html = "";
		try {

			setTrustAllCerts();
			urlObject = new URL(urldir);

			InputStreamReader isr = new InputStreamReader(urlObject.openStream());
			BufferedReader br = new BufferedReader(isr);
			while ((codigo = br.readLine()) != null) {
//				System.out.println(codigo);
				html += codigo;
			}
			br.close();
//			System.out.println(urldir);
//			System.out.println(html);
			// omitir html que tengan scripts
			if(html.contains("<script")) {
				System.out.println("Se ignora porque el HTML tienes Scripts en URL: " + urldir);
				return new ArrayList<Object>();
			}
			Document doc = Jsoup.parse(html);
			// busco todos los enlaces
			Elements entradas = doc.select("pre");
			Element elemento = entradas.first();
			String textos = elemento.html();// .text();
//			System.out.println(textos);

			String[] lineasHTML = textos.split("<br>");

//			Scanner leerHTML=new Scanner(textos);
			for (String linea : lineasHTML) {
//				System.out.println(linea);

				Document docLinea = Jsoup.parse(linea);
				linea = docLinea.text();
				// omitir la [To Parent Directory]
				if (!linea.contains("[") && !linea.contains("]")) {

					Map<String, Object> item = new HashMap<>();
					// en un principio se saca el enlace en el nombre del archivo
					Element elem = docLinea.select("a").first();
					if (elem != null) {
						item.put("nombre", elem.text());
						item.put("label", elem.text());
						item.put("url_origen", urldir + elem.text());
//						System.out.println(elem.text());
					}

					StringTokenizer leerLinea = new StringTokenizer(linea);
					while (leerLinea.hasMoreTokens()) {
						String cadena = leerLinea.nextToken();

						if (cadena.contains("/")) {
							item.put("fecha", cadena);
						} else if (cadena.equals("<dir>")) {
							item.put("tipo", "dir");
							item.put("header", "dir");
						} else {
							if (item.get("tipo") == null) {
								if (cadena.endsWith(".pdf") || cadena.endsWith(".PDF"))
									item.put("tipo", "pdf");
								else if (cadena.endsWith(".docx") || cadena.endsWith(".doc") || cadena.endsWith(".DOCX")
										|| cadena.endsWith(".DOC"))
									item.put("tipo", "doc");
								else if (cadena.endsWith(".txt") || cadena.endsWith(".TXT"))
									item.put("tipo", "txt");
								else if (cadena.endsWith(".rar") || cadena.endsWith(".RAR"))
									item.put("tipo", "rar");
								else if (cadena.toLowerCase().endsWith(".zip"))
									item.put("tipo", "zip");
								else if (cadena.toLowerCase().endsWith(".exe")||cadena.toLowerCase().endsWith(".msi"))
									item.put("tipo", "ejecutable");
								else if (cadena.endsWith(".ppt") || cadena.endsWith(".pptx") || cadena.endsWith(".PPT")
										|| cadena.endsWith(".PPTX"))
									item.put("tipo", "ppt");
								else if (cadena.toLowerCase().endsWith(".mpg") || cadena.toLowerCase().endsWith(".mp4")
										|| cadena.toLowerCase().endsWith(".wmv")
										|| cadena.toLowerCase().endsWith(".mpg"))
									item.put("tipo", "video");
								else if (cadena.toLowerCase().endsWith(".jpg") || cadena.toLowerCase().endsWith(".gif")
										|| cadena.toLowerCase().endsWith(".png")
										|| cadena.toLowerCase().endsWith(".jpeg"))
									item.put("tipo", "imagen");
								else if (cadena.endsWith(".config") || cadena.endsWith(".vsdx"))
									item.put("tipo", "sistema");
//								else
//									item.put("tipo", "file");
							}
						}

					}
					// se omite los archivos de sistemas que no se pueden leer
					if (!item.isEmpty() && item.get("tipo") != null && !item.get("tipo").equals("sistema")) {
						// se pregunta si es un directorio para buscar sus nodos
						if (item.get("tipo").equals("dir"))
							item.put("children", obtenerFullDirectorioList(urldir + item.get("nombre") + "/"));
						else
							item.put("header", "file");
						directorios.add(item);
					}
				}

			}
			return directorios;

		} catch (FileNotFoundException e) {
			System.out.println("No se pudo leer el archivo solicitado en URL: " + urldir);
			return new ArrayList<Object>();

		} catch (IOException e) {
//			e.printStackTrace();	// posible error 500 del lado del servidor
			System.out.println("No se pudo leer el archivo devuelto en URL: " + urldir);
			return new ArrayList<Object>();
		}
		catch (NullPointerException e) {
			
			e.printStackTrace();
			System.out.println("Ocurrio un error en la URL: " + urldir);
			return new ArrayList<Object>();
		}
		catch (Exception e) {
//			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * Metodo para evitar la solicitud de certificados, como solo es web scraping
	 * 
	 */
	private static void setTrustAllCerts() throws Exception {
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
			}
		} };

		// Install the all-trusting trust manager
		try {
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
				public boolean verify(String urlHostName, SSLSession session) {
					return true;
				}
			});
		} catch (Exception e) {
			// We can not recover from this exception.
			e.printStackTrace();
		}
	}
}
